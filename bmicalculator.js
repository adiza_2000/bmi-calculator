'use strict'

//bmi=w/h*2
//bmi<18,5 => underweight
//bmi intre 18.5 si 25 => normal weight
//bmi intre 25 si 30 => overweight
//bmi>30 => obesity




const weightInputElement = document.querySelector("#weight")
const heightInputElement = document.querySelector("#height")
const calculateBMIbutton = document.querySelector("#button")
const resultBMI = document.querySelector("#result")

// console.log(weightInputElement)
// console.log(heightInputElement)
// console.log(calculateBMIbutton)
// console.log(resultBMI)



function buttonClicked() {

    const weight = weightInputElement.value;
    //  console.log(weight);
    const height = heightInputElement.value;
    //  console.log(height);
    let bmi = weight / (height * height);
    //  console.log(bmi);


    if (weight == "" || height == "") {
        alert("Please input values")
    }

    if (bmi < 18.5) {
        resultBMI.innerText = "underweight";
    } else if (bmi > 18.5 && bmi < 25) {
        resultBMI.innerText = "normal weight";
    } else if (bmi > 25 && bmi < 30) {
        resultBMI.innerText = "overweight";
    } else {
        resultBMI.innerText = "obesity";
    }
}

calculateBMIbutton.addEventListener('click', buttonClicked);



